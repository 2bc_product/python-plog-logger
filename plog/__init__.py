import logging
from pythonjsonlogger import jsonlogger
import datetime


class PLogFormatter(jsonlogger.JsonFormatter):
  def format(self, record):
    setattr(record, "@timestamp", datetime.datetime.utcnow().isoformat())
    return jsonlogger.JsonFormatter.format(self, record)

def main():
  logger = logging.getLogger()
  logger.setLevel(logging.DEBUG)

  handler = logging.StreamHandler()
  handler.setFormatter(PLogFormatter())
  logger.addHandler(handler)

  logger.debug("hello", extra=dict(event="foo", t=123))

if __name__=='__main__':
  main()
