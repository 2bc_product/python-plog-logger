# PLOG

This is just a wrapper on top of [Python JSON Logger](https://github.com/madzak/python-json-logger) that enforces some rules for 2BC logging practices.

- adds an ISO8601 UTC @timestamp
