from setuptools import setup, find_packages

setup(
    name = "plog",
    version = "0.1a",
    url = "https://bitbucket.org/2bc_product/python-plog-logger",
    description = "2bc logger",
    author = "Eric Gradman",
    author_email = "egradman@twobitcircus.com",
    packages = ["plog"],
)
